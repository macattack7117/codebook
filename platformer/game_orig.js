const keys = 'RIGHT,LEFT,UP,DOWN,SPACE,W,A,S,D'
let sprite, k, pats, coin, enemy, scoreText
let score = 0

const preload = function () {
    this.load.image('bg', './assets/images/brady.png')
    this.load.image('sprite', './assets/images/white.png')
    this.load.image('blackyellow', './assets/images/blackyellow.png')
    this.load.image('enemy', './assets/images/enemy.png')
    this.load.image('coin', './assets/images/coin.png')
}

function create() {
    this.add.image(0, 0, 'bg').setOrigin(0, 0)

    plats = this.physics.add.staticGroup()
    plats.create(500, 300, 'blackyellow')

    this.add.image(200, 300, 'blackyellow')
    sprite = this.physics.add.sprite(100, 100, 'sprite')
    sprite.setScale(2.5)
    sprite.setCollideWorldBounds(true)
    sprite.setGravityY(1200)

    enemy = this.physics.add.sprite(400, 100, 'enemy')
    enemy.setScale(2.5)
    enemy.setCollideWorldBounds(true)
    enemy.setVelocity(400)
    enemy.setBounce(1)

    coins = this.physics.add.group()
    coins.create(50, 300, 'coin')

    scoreText = this.add.text(16, 16, "Score: 0", { fill: "#000", fontFamily: "Verdana", fontSize: "30px" })

    this.physics.add.collider(sprite, plats)
    this.physics.add.collider(enemy, plats)
    this.physics.add.collider(enemy, sprite)
    this.physics.add.collider(sprite, coin)

    k = this.input.keyboard.addKeys(keys)
}

function update() {

    if (k.LEFT.isDown) {
        sprite.setVelocityX(-400)
    }

    else if (k.RIGHT.isDown) {
        sprite.setVelocityX(400)
    }

    if (k.UP.isDown && sprite.body.onFloor()) {
        sprite.setVelocityY(-300)
    }

}

let config = {
    height: 384,
    width: 683,
    pixelArt: true,

    scene: { preload, create, update },
    physics: { default: 'arcade' },
}

new Phaser.Game(config)